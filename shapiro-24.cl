(defun testset (x)
  (set 'x 5)
  x)

(defun natural-numbers-from (n)
  (check-type n integer)
  (setf n (- n 1))
  #'(lambda ()
      (setf n (+ 1 n))
      n ))

;Shapiro-ex 26.2
(let ((a 0) (b 1))
  (defun fib ()
    (prog1 a (psetf a b b (+ a b)))))
;Shapiro-ex 26.3
(setf x 'origx)
(setf y 'origy)
(cond ((null x))
      ((atom y) (setf y (list y)) 'done)
      (y))
