(defvar smalls '((1 "one")
                 (2 "two")
                 (3 "three")
                 (4 "four")
                 (5 "five")
                 (6 "six")
                 (7 "seven")
                 (8 "eight")
                 (9 "nine")
                 (10 "ten")
                 (11 "eleven")
                 (12 "twelve")
                 (13 "thirteen")
                 (14 "fourteen")
                 (15 "fifteen")
                 (16 "sixteen")
                 (17 "seventeen")
                 (18 "eighteen")
                 (19 "nineteen")))

(defun num-to-string (n)
  (cond
    ((= 0 n)  (format nil ""))
    ((= n 1000 ) (format nil "onethousand"))
    ((< n 20 ) (car (cdr (nth (- n 1) smalls))))
    ((< n 30 ) (format nil "twenty~A" (num-to-string (mod n 10))))
    ((< n 40 ) (format nil "thirty~A" (num-to-string (mod n 10))))
    ((< n 50 ) (format nil "forty~A" (num-to-string (mod n 10))))
    ((< n 60 ) (format nil "fifty~A" (num-to-string (mod n 10))))
    ((< n 70 ) (format nil "sixty~A" (num-to-string (mod n 10))))
    ((< n 80 ) (format nil "seventy~A" (num-to-string (mod n 10))))
    ((< n 90 ) (format nil "eighty~A" (num-to-string (mod n 10))))
    ((< n 100) (format nil "ninety~A" (num-to-string (mod n 10))))
    ((= 0 (mod n 100)) (format nil "~Ahundred" (num-to-string (floor (/ n 100)))))
    (t (format nil "~Ahundredand~A" (num-to-string (floor (/ n 100))) (num-to-string
                                                                    (mod n 100))))
    ))

(defun solution ()
  (let ((rez 0))
    (loop for j from 1 to 1000
       do (setf rez (+ rez (length (num-to-string j)))))
    rez))

(defun factorial (n)
  (let ((rez 1))
    (loop for j from 1 to n
       do (setf rez (* rez j)))
    rez))

(defun sum-of-digits (n)
  (if (= (mod n 10) n)
      n
      (+ (mod n 10) (sum-of-digits (floor (/ n 10)))))) 






