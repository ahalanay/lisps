(defmacro ntimes (n &rest body) ;bad
  `(do ((x 0 (+ 1 x)))
       ((>= x ,n))
     ,@body))

(defmacro ntimes-good (n &rest body) ;also bad
  (let ((g (gensym)))
    `(do ((,g 0 (+ 1 ,g)))
         ((>= ,g ,n))
       ,@body)))

(let ((x 10))
  (ntimes-good 5
          (setf x (+ 1 x)))
  x)
(defmacro ntimes-really-good (n &rest body)
  (let ((g (gensym))
        (h (gensym)))
    `(let ((,h ,n))
       (do ((,g 0 (+ ,g 1)))
           ((>= ,g ,h))
         ,@body))))

(let ((v 10))
  (ntimes-really-good (setf v (- v 1))
                      (princ ".")))
;PG-ACL: Ex.10.2
(defmacro my-if (test then else)
  `(cond (,test ,then)
         (t ,else)))

;PG_ACL: Ex.10.3
(defmacro nth-expr (n &rest body)
  `(case ,n
     ,@(loop for e in body
          for n from 1
          collect `((,n) ,e))))

(let ((n 2))
  (nth-expr n (/ 1 0) (+ 1 2) (/ 1 0)))

;;PG-ACL Ex.10.4
;; see this for labels to locally define recursive functions:  http://www.gigamonkeys.com/book/the-special-operators.html
(defmacro ntimes-recursive (n &rest body)
  `(labels ((runs (i)
              (when (not (zerop i))
                ,@body
                (runs (- i 1)))))
     (runs ,n)))

(let ((v 10))
  (ntimes-recursive (setf v (- v 1))
                    (princ ".")))
;;PG-ACL Ex.10.5
(defmacro n-of (n body)
  (let ((g (gensym))
        (h (gensym)))
    `(let ((,g nil))
       (dotimes (,h ,n)
         (push ,body ,g))
       (reverse ,g))))

(let ((i 5) (n 10))
  (n-of n (incf i)))

;;PG_ACL Ex.10.6
(defmacro save-vars (v &rest body)
  `(let ,(mapcar #'(lambda (x) `(,x ,x)) v)
     ,@body))

(let ((a 0) (b 1) (c 2) (d 3))
  (format t "values before save: a=~A, b=~A, c=~A, d=~A~%" a b c d)
  (save-vars (a b c)      ;saves a b c. not d.
          (setf a (* a 10)
                b (* b 10)
                c (* c 10)
                d (* d 10))
          (format t "values in save: a=~A, b=~A, c=~A, d=~A~%" a b c d))
  (format t "values after save: a=~A, b=~A, c=~A, d=~A~%" a b c d))

;PG-ACL Ex.10.7
(defmacro push-w (obj lst)
  `(setf ,lst (cons ,obj ,lst)))

;PG-ACL Ex.10.8
(defmacro my-double (x)
  `(setf ,x (* 2 ,x)))

(let ((x 2))
  (my-double x)
  x)
