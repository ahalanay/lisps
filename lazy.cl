(deftype element ()
  '(satisfies elementp))

(deftype bstree ()
  '(satisfies bstreep))

(defun elementp (e)
  (or (characterp e) (numberp e) (packagep e) (symbolp e)))

(defun bstreep (tree)
  (or (typep tree 'element)
      (and (listp tree)
           (= (length tree) 3)
           (typep (first tree) 'element))))

(defun bstree-insert (elt tree)
  (check-type elt element)
  (cond
    ((null tree) elt)
    ((eql elt (bstree-root tree)) tree)
    ((string< elt (bstree-root tree))
     (list (bstree-root tree)
           (bstree-insert elt (bstree-left tree))
           (bstree-right tree)))
    (t (list (bstree-root tree))
       (bstree-left tree)
       (bstree-insert elt (bstree-right tree)))))
(defun bstree-root (tree)
  (check-type tree bstree)
  (if (atom tree) tree
      (first tree)))
(defun bstree-left (tree)
  (check-type tree bstree)
  (if (atom tree) '()
      (second tree)))
(defun bstree-right (tree)
  (check-type tree bstree)
  (if (atom tree) '()
      (third tree)))
(defun natural-numbers-from (n)
  (check-type n integer)
  `(,n :promise natural-numbers-from, (+ 1 n)))

(defun lazy-first (l)
  (check-type l list)
  (if (eql (first l) :promise)
      (first (eval (rest l)))
      (first l)))

(defun lazy-rest (l)
  (check-type l list)
  (if (eql (first l) :promise)
      (rest (eval (rest l)))
      (rest l)))

;;ex. 19.6
(defun lazy-nth (n l)
  (check-type n (and integer (satisfies plusp)))
  (check-type l cons)
  (if (= n 1) (lazy-first l)
      (lazy-nth (- n 1) (lazy-rest l))))

;; ex.19.7
(defun fibonacci-from (m n)
  (check-type m integer)
  (check-type n integer)
  `(,m :promise fibonacci-from ,n ,(+ m n)))

(defvar fib (fibonacci-from 0 1))


