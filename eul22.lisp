(defun get-file (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil)
          while line
       collect line)))
(defvar lst1)

(let ((in (open "names.txt")))
  (setf lst1 (format t "~a~%" (read-line in)))
  (close in)
  lst1)
