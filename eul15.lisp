(defun pows (b j)
  (loop for k from 0 to j collect (expt b j)))

(defun collatz (n)
  (if ( = n 1)
      1
      (if (evenp n)
	  (+ 1 (collatz (/ n 2)))
	  (+ 1 (collatz (+ 1 (* 3 n))))))
  )


(defun max-poz (lst)
  (let ((x (car lst))
	(i 0)
        (j 0))
    (dolist (y lst) (if (>= y x)
                        (progn  (setf x y)
                                (incf i)
                                (setf j i))
                        (incf i)))
    j))

;;(max-poz (loop for k from 1 to 1000000 collect (collatz k)))

(defun get-sol (n)
  (let ((x 1)
        (rez 1)
        (steps 1))
    (do ((j 1 (+ 1 j)))
        ((= j n) `(,rez ,x))
      (setf steps (collatz j))
      (when ( < x steps )
        (setf rez j)
        (setf x steps)))))

(defun divs (n) ;slow
  (loop for k from 1 to (/ n 2) if (= (mod n k) 0) collect k)) 

(defun divs2 (n &aux (lows '()) (highs '()))
  (do ((limit (+ 1 (isqrt n))) (factor 1 (+ 1 factor)))
      ((= factor limit)
       (when (= n (* limit limit))
         (push limit highs))
       (remove-duplicates (nreconc lows highs)))
    (multiple-value-bind (quotient remainder) (floor n factor)
      (when (zerop remainder)
        (push factor lows)
        (push quotient highs))))) ;somewhat faster

(defun primep (n)
  "Is N prime?"
  (and (> n 1) 
       (or (= n 2) (oddp n))
       (loop for i from 3 to (isqrt n) by 2
	  never (zerop (rem n i))))) ;not used

(defun faux (n)
  (let ((lst (divs2 n)))
    (let ((s 0)
          (l (length lst)))
      (do ((j 1 (+ 1 j)))
          ((> j (/ l 2)) (* 2 s))
        (let ((x (nth (- j 1) lst))
              (y (nth (- l j) lst)))
          (setf s (+ s (gcd x y)))))
      (if (oddp s)
          (setf s (+ (* 2 s) (sqrt n)))
          (* 2 s)))))

(defun f (n)
  (let ((s 0))
    (do ((j 1 (+ 1 j)))
        ((= j (+ 1 n)) s)
      (setf s (+ s (faux j)))
      ;(print j)
      )))

(defvar list-of-d)
(defun d (n)
  (apply  #' + (divs n)))

(defun cons-lod (n)
  (loop for j from 1 to n collect (d j)))

;; (defun friends (n)
;;   (setf list-of-d (cons-lod n))
;;   (let ((rez '()))
;;     (loop for k from 1 to n
;;        do (progn
;;             (print k)
;;             (loop for m from 1 to n
;;                do (if (and (= (nth (- k 1) list-of-d) m) (= (nth (- m 1) list-of-d) k)
;;                            (not (= m k)))
;;                       (progn
;;                         (setf rez (append `(,k ,m) rez))
;;                         (print "exit")
;;                         (return )
;;                         (print "exit-not"))))))
;;     rez))

(defun friends (n)
  (let (( s 0))
    (loop for k from 1 to n
       do (let ((aux (d k)))
            (if (and (> aux k) (= (d aux) k))
                (setf s (+ s (d k) k)))))
    s)) 
